![pipeline status](https://gitlab.com/wizehoodz/currency-converter/badges/master/pipeline.svg)

# currency-converter

A Node-based currency converter that supports various exchange rate providers.

## Production environment

API can be manually tested at [https://currency-converterz.herokuapp.com](http://currency-converterz.herokuapp.com)

You can choose between 3 different providers: 
- [Fixer.io](https://fixer.io/)
- [Open Exchange Rates](https://openexchangerates.org/)
- [Currency Layer](https://currencylayer.com/)

These are specified in address with `fixer`, `openex` and `clayer` suffixes. API call example:
```
https://currency-converterz.herokuapp.com/api/fixer?amount=80&input_currency=GBP&output_currency=HUF
```
OR
```
https://currency-converterz.herokuapp.com/api/openex?amount=100&input_currency=USD&output_currency=CZK
```
OR
```
https://currency-converterz.herokuapp.com/api/clayer?amount=700&input_currency=USD
```

Bear in mind that exchange rates are stored in cache (ie. Redis database) for the initial reach. Default cache expiration is set to 5 minutes, unless you flush it manually:
```
https://currency-converterz.herokuapp.com/flush
```

## Local environment 

- Install Redis database and run `redis-server`

The official instructions for various OS can be found in article "Appendix A" on their website [https://redislabs.com/ebook/appendix-a](https://redislabs.com/ebook/appendix-a/)

- Install packages
```
npm install
```

- Use the ```.env``` file inside root to set API keys

- Run CLI directly
```
node src/cli --amount <number> --input_currency <currency_code> --output_currency <currency_code>
```

- Run CLI with ```nodemon``` auto-reload
```
npm run cli -- --amount <number> --input_currency <currency_code> --output_currency <currency_code>
```

- Run API directly
```
node src/api 
```
OR
```
npm run start
```

- Run API with ```nodemon``` auto-reload
```
npm run api
```

- Run unit and integration tests
```
npm run test
```

# Project requirements (down below)

## What to do?

- CLI application
- web API application

## What do we expect?

- show us your best
- take your time, you have 2 weeks for implementation
- real-life production ready project
- you'll convert your real money with your implementation
- it's deployable
- it's tested

## Limitations

- Javascript
- all modules are allowed
- no other limitations

## Parameters
- `amount` - amount which we want to convert - float
- `input_currency` - input currency - 3 letters name or currency symbol
- `output_currency` - requested/output currency - 3 letters name or currency symbol

## Functionality
- if output_currency param is missing, convert to all known currencies

## Output
- json with following structure.
```
{
    "input": { 
        "amount": <float>,
        "currency": <3 letter currency code>
    }
    "output": {
        <3 letter currency code>: <float>
    }
}
```
## Examples

### CLI 
```
<your_app> --amount 100.0 --input_currency EUR --output_currency CZK
{   
    "input": {
        "amount": 100.0,
        "currency": "EUR"
    },
    "output": {
        "CZK": 2707.36, 
    }
}
```
```
<your_app> --amount 0.9 --input_currency ¥ --output_currency AUD
{   
    "input": {
        "amount": 0.9,
        "currency": "CNY"
    },
    "output": {
        "AUD": 0.20, 
    }
}
```
```
<your_app> --amount 10.92 --input_currency £ 
{
    "input": {
        "amount": 10.92,
        "currency": "GBP"
    },
    "output": {
        "EUR": 14.95,
        "USD": 17.05,
        "CZK": 404.82,
        .
        .
        .
    }
}
```
### API
```
GET /currency_converter?amount=0.9&input_currency=¥&output_currency=AUD HTTP/1.1
{   
    "input": {
        "amount": 0.9,
        "currency": "CNY"
    },
    "output": {
        "AUD": 0.20, 
    }
}
```

```
GET /currency_converter?amount=10.92&input_currency=£ HTTP/1.1
{
    "input": {
        "amount": 10.92,
        "currency": "GBP"
    },
    "output": {
        "EUR": 14.95,
        "USD": 17.05,
        "CZK": 404.82,
        .
        .
        .
    }
}
```
