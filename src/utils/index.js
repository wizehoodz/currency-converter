'use strict'

const currencies = require("../services/assets/currencies")

const validate = async (input = {}, output = {}) => {
    //Map the currency symbol into currency code
    if (Object.keys(currencies.map).includes(input.currency)) {
        input.currency = currencies.map[input.currency]
    }
    if (!input.currency || !currencies.list.includes(input.currency)) {
        throw new Error("Input currency is not valid!")
    }
    if (output.currency && !currencies.list.includes(output.currency)) {
        throw new Error("Output currency is not valid!")
    }

    const amount = Number(input.amount)
    if (isNaN(amount)) {
        throw new Error("Amount is not valid!")
    }
}

const calculate = async (input = {}, output = {}, rates) => {
    const result = {}
    if (output.currency) {
        const coef = rates[output.currency] / rates[input.currency]
        result[output.currency] = Number((coef * input.amount).toFixed(2))
    }
    else {
        for (const rate in rates) {
            const coef = rates[rate] / rates[input.currency]
            result[rate] = Number((coef * input.amount).toFixed(2))
        }
    }
    output.result = result

    return { input, output }
}

module.exports = { validate, calculate }