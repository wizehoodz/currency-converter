'use strict'

const Converter = require('../../services/open-exchange')
const redis = require('../../services/redis')
let converter

describe('Openex converter tests', () => {
    beforeAll(() => {
        converter = new Converter()
    })
    afterAll(async () => {
        await redis.end();
    });

    it('should return single result for defined output currency', async () => {
        const input = { amount: "100", currency: "GBP" }
        const input2 = { amount: "100", currency: "฿" }
        const output = { currency: "HUF" }

        const returned = await converter.convert(input, output)
        const returned2 = await converter.convert(input2, output)

        await expect(returned).not.toBeNull()
        await expect(Object.keys(returned.output.result).length).toEqual(1)
        await expect(returned2).not.toBeNull()
        await expect(Object.keys(returned2.output.result).length).toEqual(1)
    })

    it('should return many results for undefined output currency', async () => {
        const input = { amount: "100", currency: "GBP" }
        const input2 = { amount: "100", currency: "Bs.F." }

        const returned = await converter.convert(input)
        const returned2 = await converter.convert(input2)

        await expect(returned).not.toBeNull()
        await expect(Object.keys(returned.output.result).length).toBeGreaterThan(1)
        await expect(returned2).not.toBeNull()
        await expect(Object.keys(returned2.output.result).length).toBeGreaterThan(1)
    })

    it('should throw error for invalid input currency', async () => {
        const input = { amount: 100, currency: null }
        const input2 = { amount: 100, currency: "ZZz" }
        const output = { currency: "GBP" }

        const func = async () => await converter.convert(input, output)
        const func2 = async () => await converter.convert(input2, output)

        await expect(func).rejects.toThrowError()
        await expect(func2).rejects.toThrowError()
    })

    it('should throw error for invalid output currency', async () => {
        const input = { amount: 100, currency: "GBP" }
        const output = { currency: "HUf" }

        const func = async () => await converter.convert(input, output)

        await expect(func).rejects.toThrowError()
    })

    it('should throw error for invalid amount', async () => {
        const input = { amount: "1.500.000", currency: "GBP" }
        const input2 = { amount: "1,500", currency: "GBP" }
        const output = { currency: "USD" }

        const func = async () => await converter.convert(input, output)
        const func2 = async () => await converter.convert(input2, output)

        await expect(func).rejects.toThrowError()
        await expect(func2).rejects.toThrowError()
    })
})