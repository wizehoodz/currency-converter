'use strict'

const request = require('supertest')
const redis = require('../../services/redis')
let server

describe('API tests', () => {
    beforeEach(async () => {
        server = require('../../api')
    })
    afterEach(async () => {
        await server.close()
    })
    afterAll(async () => {
        await redis.end();
    });

    it('Fixer should return 200 for valid parameters', async () => {
        const res = await request(server).get('/api/fixer?amount=100&input_currency=USD')

        expect(res.status).toBe(200)
    })

    it('Fixer should return 500 for invalid parameters', async () => {
        const res = await request(server).get('/api/fixer?amountz=100&input_currency=USD')

        expect(res.status).toBe(500)
    })

    it('Openex should return 200 for valid parameters', async () => {
        const res = await request(server).get('/api/openex?amount=100&input_currency=USD')

        expect(res.status).toBe(200)
    })

    it('Openex should return 500 for invalid parameters', async () => {
        const res = await request(server).get('/api/openex?amountz=100&input_currency=USD')

        expect(res.status).toBe(500)
    })

    it('Clayer should return 200 for valid parameters', async () => {
        const res = await request(server).get('/api/clayer?amount=100&input_currency=USD')

        expect(res.status).toBe(200)
    })

    it('Clayer should return 500 for invalid parameters', async () => {
        const res = await request(server).get('/api/clayer?amountz=100&input_currency=USD')

        expect(res.status).toBe(500)
    })
})