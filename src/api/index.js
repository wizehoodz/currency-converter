'use strict'

require('dotenv').config()
const express = require('express')
const app = express()
const port = process.env.PORT || 3000

const { FixerConverter, OpenExchangeConverter, CurrencyLayerConverter, redis } = require('../services')
const services = {
    fixer: new FixerConverter(),
    openex: new OpenExchangeConverter(),
    clayer: new CurrencyLayerConverter()
}

app.get('/', async (req, res) => {
    try {
        res.status(200).send("API started!")
    }
    catch (ex) {
        res.status(500).send(ex.message)
    }
})

app.get('/flush', async (req, res) => {
    try {
        await redis.flushall();
        res.status(200).send("Redis cache flushed!")
    }
    catch (ex) {
        res.status(500).send(ex.message)
    }
})

app.get('/api/:provider', async (req, res) => {
    try {
        if (!Object.keys(services).includes(req.params.provider)) {
            res.status(400).send("Bad provider, please try again.")
        }
        console.log(`GET ${req.params.provider} started: `, { query: req.query })

        const { amount, input_currency, output_currency } = req.query
        const input = { amount, currency: input_currency }
        const output = { currency: output_currency }

        const result = await services[req.params.provider].convert(input, output)
        res.status(200).send(result)

        console.log(`GET ${req.params.provider} finished: `, result.output)
    }
    catch (ex) {
        res.status(500).send(ex.message)
        console.log("GET error: ", { message: ex.message })
    }
})

const server = app.listen(port, () => console.log(`API listening at port ${port}...`))

module.exports = server