'use strict'

require('dotenv').config()
const axios = require('axios')
const redis = require("../redis")
const { validate, calculate } = require("../../utils")

class Converter {
    constructor() {
        if (!process.env.OPENEX_API_KEY) {
            throw new Error("OPENEX_API_KEY not found!")
        }
        if (!process.env.CACHE_EXPIRATION_SEC) {
            throw new Error("CACHE_EXPIRATION_SEC not found!")
        }
        if (isNaN(process.env.CACHE_EXPIRATION_SEC)) {
            throw new Error("CACHE_EXPIRATION_SEC is invalid!")
        }
    }

    async convert(input, output) {
        await validate(input, output)

        let data = await redis.get("rates:openex")
        if (!data) {
            const result = await axios.get(`https://openexchangerates.org/api/latest.json?app_id=${process.env.OPENEX_API_KEY}`)
            data = result.data
            
            await redis.setex("rates:openex", eval(process.env.CACHE_EXPIRATION_SEC), JSON.stringify(data))
            console.log("Data retrieved from API")
        }
        else {
            data = JSON.parse(data)
            console.log("Data retrieved from Redis")
        }

        if (data) {
            const result = await calculate(input, output, data.rates)
            return result
        }
        else return null
    }
}

module.exports = Converter