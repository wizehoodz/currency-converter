const FixerConverter = require('./fixer');
const OpenExchangeConverter = require('./open-exchange');
const CurrencyLayerConverter = require('./currency-layer');
const redis = require('./redis');

module.exports = { FixerConverter, OpenExchangeConverter, CurrencyLayerConverter, redis }