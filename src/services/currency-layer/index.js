'use strict'

require('dotenv').config()
const axios = require('axios')
const redis = require("../redis")
const { validate, calculate } = require("../../utils")

class Converter {
    constructor() {
        if (!process.env.CLAYER_API_KEY) {
            throw new Error("CLAYER_API_KEY not found!")
        }
        if (!process.env.CACHE_EXPIRATION_SEC) {
            throw new Error("CACHE_EXPIRATION_SEC not found!")
        }
        if (isNaN(process.env.CACHE_EXPIRATION_SEC)) {
            throw new Error("CACHE_EXPIRATION_SEC is invalid!")
        }
    }

    async convert(input, output) {
        await validate(input, output)

        let data = await redis.get("rates:clayer")
        if (!data) {
            const result = await axios.get(`http://api.currencylayer.com/live?access_key=${process.env.CLAYER_API_KEY}`)
            const quotes = result.data.quotes
            for (const quote in quotes) {
                const newCode = quote.replace('USD', '')
                quotes[newCode] = quotes[quote]
                delete quotes[quote]
            }
            data = result.data

            await redis.setex("rates:clayer", eval(process.env.CACHE_EXPIRATION_SEC), JSON.stringify(data))
            console.log("Data retrieved from API")
        }
        else {
            data = JSON.parse(data)
            console.log("Data retrieved from Redis")
        }
        if (data) {
            const result = await calculate(input, output, data.quotes)
            return result
        }
        else return null
    }
}

module.exports = Converter