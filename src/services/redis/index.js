'use strict'

const Redis = require("ioredis");

let client
if (process.env.NODE_ENV === "production") {
    if (!process.env.REDIS_URL) {
        throw new Error("REDIS_URL not found!")
    }
    client = new Redis(process.env.REDIS_URL)
}
else {
    const port = process.env.REDIS_PORT || 6379;
    client = new Redis(port)
}

module.exports = client

