'use strict'

require('dotenv').config()
const fixer = require("fixer-api")
const redis = require("../redis")
const { validate, calculate } = require("../../utils")
/*
    This one could be also worth implementing instead of providing API key
    https://exchangeratesapi.io/
    https://github.com/exchangeratesapi/exchangeratesapi
*/

class Converter {
    constructor() {
        if (!process.env.FIXER_API_KEY) {
            throw new Error("FIXER_API_KEY not found!")
        }
        if (!process.env.CACHE_EXPIRATION_SEC) {
            throw new Error("CACHE_EXPIRATION_SEC not found!")
        }
        if (isNaN(process.env.CACHE_EXPIRATION_SEC)) {
            throw new Error("CACHE_EXPIRATION_SEC is invalid!")
        }
        fixer.set({ accessKey: process.env.FIXER_API_KEY })
    }

    async convert(input, output) {
        await validate(input, output)

        let data = await redis.get("rates:fixer")
        if (!data) {
            data = await fixer.latest()

            await redis.setex("rates:fixer", eval(process.env.CACHE_EXPIRATION_SEC), JSON.stringify(data))
            console.log("Data retrieved from API")
        }
        else {
            data = JSON.parse(data)
            console.log("Data retrieved from Redis")
        }

        if (data.success) {
            const result = await calculate(input, output, data.rates)
            return result
        }
        else return null
    }
}

module.exports = Converter

