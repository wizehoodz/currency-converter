'use strict'

const { FixerConverter, redis } = require('../services')
var args = require('minimist')(process.argv.slice(2))

const main = async () => {
    const converter = new FixerConverter()

    const input = {
        amount: args.amount,
        currency: args.input_currency
    }
    const output = {
        currency: args.output_currency
    }

    const result = await converter.convert(input, output)
    console.log(JSON.stringify(result, null, 2))

    await redis.end()
}

main()
